# SRB2 Master Server

This has been separated from the core SRB2 repo for cleanliness. It needs to be updated with documentation!

# License

Copyright (C) Sonic Team Junior, 2015

This code is made available under the terms of the GNU General Public License, version 2. You can see that license [here](http://www.gnu.org/licenses/gpl-2.0.html).